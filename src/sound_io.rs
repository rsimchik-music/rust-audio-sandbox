extern crate cpal;

use cpal::{traits::{DeviceTrait, HostTrait, StreamTrait}, SupportedBufferSize};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;
pub struct CpalOutputConfig<'a> {
    pub host: cpal::Host,
    pub output: &'a cpal::Device,
    pub stream_config: cpal::StreamConfig,
}

pub struct HostOptions {
    pub host_names: Vec<String>,
    pub default_host_name: String,

    _hosts: Vec<cpal::HostId>,
}

pub struct OutputOptions {
    pub output_names: Vec<String>,
    pub default_output_name: String,

    _outputs: Vec<cpal::Device>,
}

#[derive(Default, Clone, Copy)]
pub struct OutputConfigFilters {
    pub channel_count: Option<u16>,
    pub sample_rate: Option<u32>,
    pub buffer_size: Option<u32>,
    pub bit_depth: Option<cpal::SampleFormat>,
}
impl OutputConfigFilters {
    pub fn are_all_some(&self) -> bool {
        self.channel_count.is_some() &&
        self.sample_rate.is_some() &&
        self.buffer_size.is_some() &&
        self.bit_depth.is_some()
    }
}
impl ToString for OutputConfigFilters {
    fn to_string(&self) -> String {
        let not_found = "?";

        let mut result = String::new();

        result.push_str("Channel Count: ");
        result.push_str(&self.channel_count.map(|cc| cc.to_string())
            .unwrap_or(not_found.to_string())
        );

        result.push_str("\nSample Rate: "); 
        result.push_str(&self.sample_rate.map(|sr| sr.to_string())
            .unwrap_or(not_found.to_string())
        );

        result.push_str("\nBuffer Size: ");
        result.push_str(&self.buffer_size.map(|bs| bs.to_string())
            .unwrap_or(not_found.to_string())
        );

        result.push_str("\nBit Depth: ");
        result.push_str(&self.bit_depth.map(|bd| bd.to_string())
            .unwrap_or(not_found.to_string())
        );

        result
    }
}
impl From<OutputConfigFilters> for cpal::StreamConfig {
    fn from(val: OutputConfigFilters) -> Self {
        cpal::StreamConfig {
            channels: val.channel_count.unwrap(),
            sample_rate: cpal::SampleRate(val.sample_rate.unwrap()),
            buffer_size: cpal::BufferSize::Fixed(val.buffer_size.unwrap()),
        }
    }
}

pub fn create_stream(
    host_name: &str,
    output_name: &str,
    stream_options: OutputConfigFilters,
    process: impl FnMut(&mut [f32]) + Send + 'static
) -> Result<cpal::Stream> {
    let host_options = scan_hosts();
    let host = get_cpal_host(&host_options, host_name)?;

    let output_options = scan_outputs(&host)?;
    let output = get_cpal_output(&output_options, output_name)?;

    let all_configs: Vec<cpal::SupportedStreamConfigRange> = output.supported_output_configs().unwrap().collect();
    if filter_cpal_configs(&all_configs, &stream_options).len() == 0 {
        return Err("No valid output config found.".into());
    }

    init_stream(output, stream_options, process)
}


pub fn scan_hosts () -> HostOptions {
    let hosts = cpal::available_hosts();
    let default_host_name = String::from(cpal::default_host().id().name());
    let host_names = hosts.iter()
        .map(|host| String::from(host.name()))
        .collect::<Vec<_>>();

    HostOptions {
        host_names,
        default_host_name,
        _hosts: hosts,
    }
}

// WARNING: this function prints to stdout - out of our control
pub fn scan_outputs(host: &cpal::Host) -> Result<OutputOptions> {
    let default_output_name = host.default_output_device()
        .ok_or("No output device available")?
        .name()?;

    let mut output_names: Vec<String> = Vec::new();
    let mut outputs: Vec<cpal::Device> = Vec::new();
    for output in host.output_devices()? {
        let name = output.name()?;
        output_names.push(name);
        outputs.push(output);
    }

    Ok(OutputOptions {
        _outputs: outputs,
        output_names,
        default_output_name,
    })
}

pub fn get_cpal_host(host_options: &HostOptions, selected_host_name: &str) -> Result<cpal::Host> {
    host_options._hosts
        .iter().find(|id| id.name() == selected_host_name)
        .ok_or_else(|| format!("Invalid host name: {}", selected_host_name).into())
        .and_then(|id| cpal::host_from_id(*id)
            .map_err(|_| format!("Host unavailable: {}", selected_host_name).into())
        )
}

pub fn get_cpal_output<'a>(output_options: &'a OutputOptions, selected_output_name: &str) -> Result<&'a cpal::Device> {
    if selected_output_name.is_empty() {
        return Err("No output selected.".into());
    }

    output_options._outputs
        .iter().find(|output| output.name().unwrap_or("".to_string()) == selected_output_name)
        .ok_or_else(|| format!("Invalid output name: {}", selected_output_name).into())
}

pub fn filter_cpal_configs<'a>(
    configs: &'a Vec<cpal::SupportedStreamConfigRange>, 
    filters: &OutputConfigFilters
) -> Vec<&'a cpal::SupportedStreamConfigRange> {
    configs.iter().filter(|config| {
        if let Some(channel_count) = filters.channel_count {
            if config.channels() != channel_count {
                return false;
            }
        }

        if let Some(sample_rate) = filters.sample_rate {
            if sample_rate > config.max_sample_rate().0 || sample_rate < config.min_sample_rate().0 {
                return false;
            }
        }

        if let Some(buffer_size) = filters.buffer_size {
            let (min, max) = match config.buffer_size() {
                SupportedBufferSize::Range{min, max} => (min, max),
                SupportedBufferSize::Unknown => return false,
            };

            if buffer_size > *max || buffer_size < *min {
                return false;
            }
        }

        if let Some(bit_depth) = filters.bit_depth {
            if bit_depth != config.sample_format() {
                return false;
            }
        }

        true
    }).collect::<Vec<_>>()
}

pub fn init_stream(
    output: &cpal::Device, 
    config: OutputConfigFilters, 
    mut process: impl FnMut(&mut [f32]) + Send + 'static
) -> Result<cpal::Stream> {
    let stream = output.build_output_stream(
        &config.into(),
        move |data: &mut [f32], _: &cpal::OutputCallbackInfo| {
            process(data);
        },
        move |err| {
            eprintln!("ERROR: {}", err);
        },
        None
    ).expect("Could not build output stream.");

    stream.play()?;
    Ok(stream)
}

/*  Nestable integer clock. Ticking a clock also ticks the clock's children, if it has any.
    A child clock may not be faster than its parent. */
pub struct Clock {
    pub name: &'static str,
    pub value: u64,
    pub min_value: u64,
    pub max_value: Option<u64>,
    pub ticks_per_beat: u64,

    just_ticked: bool,
    children: Vec<Clock>,
}
impl<'a> Clock {
    pub fn new(
        name: &'static str, 
        min_value: u64, 
        max_value: Option<u64>, 
        ticks_per_beat: u64
    ) -> Clock {
        Clock {
            name,
            value: min_value,
            min_value,
            ticks_per_beat,
            max_value,
            just_ticked: false,
            children: Vec::new(),
        }
    }

    pub fn tick(&mut self) {
        self.just_ticked = true;
        self.value += 1;
        if let Some(max_value) = self.max_value {
            if self.value >= max_value {
                self.value = self.min_value;
            }
        }

        for child in &mut self.children {
            child.parent_ticked(self.value);
        }
    }

    pub fn just_ticked(&self) -> bool {
        self.just_ticked
    }

    pub fn push_child(&mut self, child: Clock) -> Result<()> {
        if child.ticks_per_beat < self.ticks_per_beat {
            return Err("Child clock must tick at least as fast as its parent.".into());
        }

        self.children.push(child);
        Ok(())
    }

    pub fn get_child(&self, name: &str) -> Option<&Clock> {
        self.children.iter().find(|child| child.name == name)
    }


    fn parent_ticked(&mut self, value: u64) {
        self.just_ticked = false;

        if (value % self.ticks_per_beat) == 0 {
            self.tick();
        }
    }
}

#[derive(Clone)]
pub struct TestToneState {
    pub frequency: f32,
    pub master_gain: f32,
    pub gain: f32,
    pub signal: bool,
    pub config: cpal::StreamConfig,
}
impl TestToneState {
    pub fn get_sample(&mut self, clock: u64) -> f32 {
        let sample_rate = self.config.sample_rate.0 as f32;
        let wavelength_samples = (sample_rate / self.frequency) * (self.config.channels as f32);
        let index = clock % wavelength_samples as u64;
        self.signal = index < (wavelength_samples / 2.0) as u64;

        return if self.signal { self.gain * self.master_gain } else { -self.gain * self.master_gain };
    }
}