pub fn midinote_to_freq(midinote: u8) -> f32 {
	const NOTES_PER_OCTAVE: u8 = 12;
	const A4_FREQ: f32 = 440.0;
	const A4_MIDI: i16 = 69;

	let semitone_ratio = semitone_ratio(NOTES_PER_OCTAVE);
	let note_diff: i16 = i16::from(midinote) - A4_MIDI;
	return A4_FREQ * semitone_ratio.powf(f32::from(note_diff));
}

pub fn semitone_ratio(notes_per_octave: u8) -> f32 {
	return 2_f32.powf(1.0 / (notes_per_octave as f32));
}

// preserves the proportional value of the 8bit value in a 16bit value
pub fn u8_to_u16(input: u8) -> u16 {
	u16::from(input) * 257
}

pub fn samples_per_beat(bpm: f32, sample_rate: u32) -> u64 {
	(sample_rate as f32 / (bpm / 60.0)) as u64
}

#[cfg(test)]
mod tests {
	use approx::relative_eq;
	use super::*;

	#[test]
	fn midinote_to_freq_high() {
		const E7: u8 = 100;
		const E7_FREQ: f32 = 2637.0;

		let result = midinote_to_freq(E7);
		relative_eq!(result, E7_FREQ, epsilon = f32::EPSILON);
	}

	#[test]
	fn midinote_to_freq_low() {
		const E1: u8 = 28;
		const E1_FREQ: f32 = 41.203;

		let result = midinote_to_freq(E1);
		relative_eq!(result, E1_FREQ, epsilon = f32::EPSILON);
	}

	#[test]
	fn u8_to_u16_high() {
		const INPUT: u8 = u8::max_value();
		const EXPECTED: u16 = u16::max_value();

		let result = u8_to_u16(INPUT);
		assert_eq!(result, EXPECTED);
	}

	#[test]
	fn u8_to_u16_low() {
		const INPUT: u8 = u8::min_value();
		const EXPECTED: u16 = u16::min_value();

		let result = u8_to_u16(INPUT);
		assert_eq!(result, EXPECTED);
	}

	#[test]
	fn u8_to_u16_mid() {
		const INPUT: u8 = 128;
		const EXPECTED: u16 = 32896;

		let result = u8_to_u16(INPUT);
		assert_eq!(result, EXPECTED);
	}

	fn samples_per_beat_120bpm() {
		const BPM: f32 = 120.0;
		const SAMPLE_RATE: u32 = 44100;
		const EXPECTED: u64 = 22050;

		let result = samples_per_beat(BPM, SAMPLE_RATE);
		assert_eq!(result, EXPECTED);
	}
}
