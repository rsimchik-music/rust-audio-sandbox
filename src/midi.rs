extern crate midir;

use std::collections::HashMap;
use std::error::Error;
pub type Result<T> = std::result::Result<T, Box<dyn Error>>;

#[derive(Debug, Clone)]
pub struct MidiNote {
    pub pitch: u8,
    pub volume: u8,
    pub start: u64,
    pub end: Option<u64>,
}

pub struct MidiInput {
    pub port_names: Vec<String>,
    _input: midir::MidiInput,
    _ports: midir::MidiInputPorts,
}
impl MidiInput {
    pub fn scan() -> Result<MidiInput> {
        let mut midi_input = midir::MidiInput::new("").unwrap();
        let midi_in_ports = midi_input.ports();
        let port_names = midi_in_ports.iter()
            .map(|port| midi_input.port_name(port).unwrap_or("N/A".to_string()))
            .collect();

        Ok(MidiInput {
            port_names,
            _input: midi_input,
            _ports: midi_in_ports,
        })
    }

    pub fn open_stream<T: Send>(
        self,
        input_index: usize,
        stream_name: &str,
        data: T,
        callback: impl FnMut(u64, &[u8], &mut T) + Send + 'static
    ) -> Result<midir::MidiInputConnection<T>> {
        let in_port = self._ports.get(input_index).unwrap();
        let in_port_name = self._input.port_name(in_port).unwrap();

        self._input.connect(in_port, stream_name, callback, data)
            .map_err(|e| format!("Failed to open midi input stream: {}", e).into())
    }
}


pub trait MidiHandler {
    // Return value indicates whether or not the message was handled successfully.
    fn handle(&mut self, timestamp: u64, message: &[u8]) -> Result<()>;
}

pub struct MidiLogger { }
impl MidiHandler for MidiLogger {
    fn handle(&mut self, timestamp: u64, message: &[u8]) -> Result<()> {
        println!("{}: {:?} (len = {})", timestamp, message, message.len());
        Ok(())
    }
}

const MIDI_NOTE_ON: u8 = 144;
const MIDI_NOTE_OFF: u8 = 128;
#[derive(Debug)]
pub struct MidiNoteReader {
    pub active_notes: HashMap<u8, MidiNote>,
    pub note_history: Vec<MidiNote>,
}
impl MidiNoteReader {
    pub fn new() -> MidiNoteReader {
        MidiNoteReader {
            active_notes: HashMap::new(),
            note_history: Vec::new(),
        }
    }
}
impl MidiHandler for MidiNoteReader {
    fn handle(&mut self, timestamp: u64, message: &[u8]) -> Result<()> {
        let event = message[0];
        if message.len() != 3 || ![MIDI_NOTE_ON, MIDI_NOTE_OFF].contains(&event) {
        }

        let pitch = message[1];
        let volume = message[2];
        if event == MIDI_NOTE_ON {
            if self.active_notes.contains_key(&pitch) {
                return Err("Recieved 'note on' signal for already active note".into());
            }
            
            // TODO convert u8 to u16 proportional to the max value of u16
            let note = MidiNote { pitch, volume, start: timestamp, end: None };
            self.active_notes.insert(pitch, note);
        } else if message.len() == 3 && message[0] == MIDI_NOTE_OFF {
            let mut note = self.active_notes.remove(&pitch)
                .ok_or("Recieved 'note off' signal without matching 'note on' signal")?;
            note.end = Some(timestamp);
            self.note_history.push(note);
        }

        Ok(())
    }
}
