const NOTES_PER_OCTAVE: u16 = 12;

#[derive(Clone, Copy)]
pub struct Note {
    pub pitch: u16,
    pub volume: f32,
}
impl Note {
    pub fn zip(pitches: &[u16], volumes: &[f32]) -> Vec<Note> {
        let mut notes: Vec<Note> = Vec::new();
        for (i, pitch) in pitches.iter().enumerate() {
            notes.push(
                Note {
                    pitch: *pitch,
                    volume: volumes[i % volumes.len()],
                }
            );
        }
        notes
    }
}


const DIATONIC_INTERVALS: [u16; 7] = [0, 2, 4, 5, 7, 9, 11];
#[derive(Clone, Copy)]
pub struct DiatonicScale {
    pub root: u16,
    pub mode: DiatonicMode,
}
impl DiatonicScale {
    pub fn new(root: u16, mode: DiatonicMode) -> DiatonicScale {
        DiatonicScale {
            root,
            mode,
        }
    }

    pub fn render(&self) -> Vec<u16> {
        let final_intervals = apply_mode(self.mode.into(), &DIATONIC_INTERVALS);
        final_intervals.iter().map(|interval| {
            self.root + interval
        }).collect()
    }
}

#[derive(Clone, Copy)]
pub enum DiatonicMode {
    Ionian = 0,
    Dorian = 1,
    Phrygian = 2,
    Lydian = 3,
    Mixolydian = 4,
    Aeolian = 5,
    Locrian = 6,
}
impl DiatonicMode {
    pub const fn major() -> DiatonicMode { DiatonicMode::Ionian }
    pub const fn minor() -> DiatonicMode { DiatonicMode::Aeolian }
}
impl Into<u16> for DiatonicMode {
    fn into(self) -> u16 {
        self as u16
    }
}

/*  Most common chords can be built by stacking thirds (or even other intervals)
 *  on top of a root note. e.g. C -> Cmaj7 -> 10th -> 13th ... */
#[derive(Clone)]
pub struct DiatonicChord {
    pub parent_scale: Vec<u16>,
    pub root: u16,  // 1-indexed
    pub interval: u16,  // 1-indexed
    pub size: u16,
}
impl DiatonicChord {
    pub fn render(&self) -> Vec<u16> {
        let scale = &self.parent_scale;
        let i_interval = self.interval - 1;
        let i_root = self.root - 1;

        let mut notes: Vec<u16> = Vec::new();
        for i in 0..self.size {
            let degree = (i_root + i * i_interval) as usize;
            let octave = (degree / scale.len()) as u16;
            notes.push(scale[degree % scale.len()] + octave * NOTES_PER_OCTAVE);
        }
        notes
    }
}

#[derive(Clone)]
pub struct Arpeggiator {
    pub scale: Vec<u16>,
    pub chords: Vec<DiatonicChord>,
    pub gains: Vec<f32>,
    pub beats_per_chord: u64,
    
    pub channels: u16,
    pub sample_rate: u32,
    
    chord_clock: u64,
    sequence: Vec<Note>
}
impl Arpeggiator {
    pub fn new(
        scale: Vec<u16>,
        chords: Vec<DiatonicChord>,
        gains: Vec<f32>,
        beats_per_chord: u64,
        channels: u16,
        sample_rate: u32,
    ) -> Arpeggiator {
        let mut state = Arpeggiator {
            channels,
            sample_rate,
            scale,
            chords,
            gains,
            beats_per_chord,
            chord_clock: 0,
            sequence: Vec::new(),
        };
        state.sequence = Note::zip(state.chords[0].render().as_slice(), &state.gains);
        state
    }
    pub fn tick(&mut self, beat_clock: u64) -> Note {
        if beat_clock % self.beats_per_chord as u64 == 0 {
            self.chord_clock += 1;
            if self.chord_clock >= self.chords.len() as u64 {
                self.chord_clock = 0;
            }
            let new_chord = &self.chords[self.chord_clock as usize];
            self.sequence = Note::zip(new_chord.render().as_slice(), &self.gains);
        }

        let i_seq = beat_clock as usize % self.sequence.len();
        self.sequence[i_seq].clone()
    }
}

pub fn apply_mode(mode: u16, scale: &[u16]) -> Vec<u16> {
    if mode == 0 {
        scale.into()
    } else {
        let i_root = mode as usize;
        let root_note = scale[i_root];
        let transformed_notes = scale.iter().enumerate().map(|(i,_)| { 
            let i_shifted = (i + i_root) % scale.len();
            let mut shifted_value = scale[i_shifted];
            if shifted_value < root_note {
                shifted_value += NOTES_PER_OCTAVE;
            }
            shifted_value - root_note
        }).collect();

        transformed_notes
    }
}
