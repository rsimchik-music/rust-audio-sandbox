pub mod music_theory;
pub mod nodes;
pub mod tui;
pub mod sound_io;
pub mod conversion;
pub mod midi;

use midi::MidiHandler;

extern crate cpal;
use cpal::traits::StreamTrait;

use std::error::Error;
pub type Result<T> = std::result::Result<T, Box<dyn Error>>;

fn main() {
    let midi_input = midi::MidiInput::scan().unwrap();
    let midi_stream = midi_input.open_stream::<midi::MidiNoteReader>(
        1,
        "synth-input",
        midi::MidiNoteReader::new(),
        move |stamp, message, reader| {
            reader.handle(stamp, message).expect("Failed to handle midi message");
            println!("{:?}", reader);
        }
    ).unwrap();

    let config = sound_io::OutputConfigFilters {
        channel_count: Some(2),
        sample_rate: Some(48000),
        buffer_size: Some(256),
        bit_depth: Some(cpal::SampleFormat::F32),
    };

    let mut tone = sound_io::TestToneState {
        master_gain: 0.1,
        frequency: 440.0,
        gain: 1.0,
        signal: false,
        config: config.into(),
    };

    let mut sample_clock = sound_io::Clock::new("sample", 0, None, 1);
    let output_stream = sound_io::create_stream(
        "ALSA",
        "default",
        config,
        move |data: &mut [f32]| {
            for sample in data.iter_mut() {
                *sample = tone.get_sample(sample_clock.value);
                sample_clock.tick();
            }
        }
    ).expect("Could not create stream.");

    println!("Press <enter> to stop.");
    tui::wait_for_enter();
    output_stream.pause().expect("Could not pause stream.");
    midi_stream.close();
}
